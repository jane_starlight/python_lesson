﻿"""
== Лото ==
Правила игры в лото.
Игра ведется с помощью специальных карточек, на которых отмечены числа,
и фишек (бочонков) с цифрами.
Количество бочонков — 90 штук (с цифрами от 1 до 90).
Каждая карточка содержит 3 строки по 9 клеток. В каждой строке по 5 случайных цифр,
расположенных по возрастанию. Все цифры в карточке уникальны. Пример карточки:
--------------------------
    9 43 62          74 90
 2    27    75 78    82
   41 56 63     76      86
--------------------------
В игре 2 игрока: пользователь и компьютер. Каждому в начале выдается
случайная карточка.
Каждый ход выбирается один случайный бочонок и выводится на экран.
Также выводятся карточка игрока и карточка компьютера.
Пользователю предлагается зачеркнуть цифру на карточке или продолжить.
Если игрок выбрал "зачеркнуть":
	Если цифра есть на карточке - она зачеркивается и игра продолжается.
	Если цифры на карточке нет - игрок проигрывает и игра завершается.
Если игрок выбрал "продолжить":
	Если цифра есть на карточке - игрок проигрывает и игра завершается.
	Если цифры на карточке нет - игра продолжается.

Побеждает тот, кто первый закроет все числа на своей карточке.
Пример одного хода:
Новый бочонок: 70 (осталось 76)
------ Ваша карточка -----
 6  7          49    57 58
   14 26     -    78    85
23 33    38    48    71
--------------------------
-- Карточка компьютера ---
 7 11     - 14    87
      16 49    55 77    88
   15 20     -       76  -
--------------------------
Зачеркнуть цифру? (y/n)
Подсказка: каждый следующий случайный бочонок из мешка удобно получать
с помощью функции-генератора.
Подсказка: для работы с псевдослучайными числами удобно использовать
модуль random: http://docs.python.org/3/library/random.html
"""
import random as r
import sys
import os

class Card():
    """
    Класс "Карточка"
    """
    def __init__(self, name):
        """
        Инициализация карточки
        :param name: имя владельца карточки
        """
        self.name = name    # Задаём имя игрока
        bbl = [i for i in range(1, 91)]  # Мешок с бочками
        # Создаём таблицу с числами
        self.table = [
            __class__.Generate(self, bbl),    # Задём 1-ую строку
            __class__.Generate(self, bbl),    # Задём 2-ую строку
            __class__.Generate(self, bbl)     # Задём 3-ую строку
        ]

    def Generate(self, bbl):
        """
        Метод для генерации строки карточки
        :param bbl: массив номер бочонков
        :return: строку таблицы карточки
        """
        line = ['_' for i in range(9)]   # Создаём пустую строку
        numb = []   # Задаём массив чисел строки
        for i in range(5):  # Выбираем 5 чисел из бочонка
            numb.append(bbl.pop(r.randint(0, len(bbl) - 1)))
        for i in range(4):  # Заполняем пустые ячейки
            flag = True
            while flag:
                ind = r.randint(0, len(line) - 1)   # Выбираем случайный индекс
                if line[ind] == '_':
                    line[ind] = ''
                    flag = False
        numb.sort()     # Сортируем числа
        ind = 0     # Индекс массива числа
        for i in range(9):  # Заполняем строку числами
            if line[i] != '':
                line[i] = numb[ind]
                ind += 1
        return line

    def __str__(self):
        """
        Метод для print для вывода карточки
        :return: вывод карточки
        """
        rez = '{:-^26}\n'.format(self.name)
        for x in range(3):
            rez += '{:>2} {:>2} {:>2} {:>2} {:>2} {:>2} {:>2} {:>2} {:>2}'.format(*self.table[x]) + '\n'
        return rez + ('-'*26)

    def Check(self, num):
        """
        Метод для проверки карточки
        :param num: номер бочонка
        :return: True, если номер на карточки присутствует, иначе False
        """
        for i in range(3):
            for j in range(9):
                if self.table[i][j] == num:
                    return True
        return False

    def CrossOut(self, num):
        """
        Метод зачёркивания числа
        :param num: номер бочонка
        """
        for i in range(3):
            for j in range(9):
                if self.table[i][j] == num:
                    self.table[i][j] = '-'
                    break

    def Winner(self):
        """
        Метод для проверки на полное заполнение карточки
        :return: True, если карточка заполнена, иначе False
        """
        count = 0
        for i in range(3):
            for j in range(9):
                if self.table[i][j] == '-':
                    count += 1
        if count == 15:
            return True
        return False

    def getName(self):
        """
        Метод для возвращения имени владельца карточки
        :return: имя владельца карточки
        """
        return self.name


player = Card("Ваша карточка")
computer = Card("Карточка компьютера")

bbl = [x for x in range(1, 91)]  # Создаём мешок с бочонками

# Запускаем игру
while True:
    barrel = bbl.pop(r.randint(0, len(bbl) - 1))    # Вытягиваем бочонок
    print("\nНовый бочонок: {} (осталось {})".format(barrel, len(bbl)))     # Сообщаем номер бочонка и кол-во оставшихся
    print(player)   # Выводим карточку игрока
    print(computer)     # Выводим карточку компьютера

    q = input("Зачеркнуть цифру? (y/n)").lower()    # Спрашиваем ответ
    if q == 'y':
        if player.Check(barrel):
            player.CrossOut(barrel)
        else:
            print("Вы проиграли! В вашей карточке нет такого числа!")
            os.system("pause")
            sys.exit(0)
    elif q == 'n':
        if player.Check(barrel):
            print("Вы проиграли! В вашей карточке есть такое число!")
            os.system("pause")
            sys.exit(0)
        else:
            player.CrossOut(barrel)
    else:
        print("Некорректный ввод! Вы проиграли!")
        os.system("pause")
        sys.exit(0)

    computer.CrossOut(barrel)

    if player.Winner():
        print("{} победил".format(player.getName()))
        os.system("pause")
        sys.exit(0)
    elif computer.Winner():
        print("{} победил".format(computer.getName()))
        os.system("pause")
        sys.exit(0)
