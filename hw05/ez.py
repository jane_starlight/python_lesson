import os


def pereiti_in_dir(path, name):
    """
    Функция для перещение в другую папку
    :param path: Название директории
    :return:
    """
    try:
        dpath = os.path.join(path, name)
        os.chdir(dpath)
        print("Успешно перешли в", name, "директорию\n")
    except FileNotFoundError:
        print('Данной диретории не существует')


def dirrectoria_dir(path=os.getcwd()):
    """
    Функция для просмотра содержимого текущей папки
    :param path: Дирректория
    :return: Список папок дирректории
    """
    return [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]


def remove_dir(dir_name):
    """
    Функция для удаления указаной директории
    :param dir_name: Название директории
    """
    try:
        os.removedirs(dir_name)
        print("Удаление директории", dir_name, "успешно произошло\n")
    except FileNotFoundError:
        print('Директория отсутствует')


def create_dir(path, name):
    """
    Функция для создания папки
    :param path: Текущий путь, где будет создана директория
    :param name: Название директории
    """
    try:
        dpath = os.path.join(path, name)
        os.mkdir(dpath)
        print("Создание директории", name, "успешно произошло\n")
    except FileExistsError:
        print('Данная директория уже существует')
