__author__ = "Кувшинова Евгения"

# Задание-1:
# Напишите функцию, возвращающую ряд Фибоначчи с n-элемента до m-элемента.
# Первыми элементами ряда считать цифры 1 1

print("Задание 1 - Фибоначчи")
import os
path = os.path.join('text.txt')


def fibonacci(n, m):
    fib = [0, 0]
    mass = []
    for i in range(m):
        if i < 2:
            temp = fib[0] + 1
        else:
            temp = fib[0] + fib[1]
        fib = [fib[1], temp]
        if i + 1 >= n:
            mass.append(temp)
    return mass

with open(path, 'r', encoding='UTF-8') as fw:
    n = int(fw.readline())
    m = int(fw.readline())
print(fibonacci(n, m))



# Задача-2:
# Напишите функцию, сортирующую принимаемый список по возрастанию.
# Для сортировки используйте любой алгоритм (например пузырьковый).
# Для решения данной задачи нельзя использовать встроенную функцию и метод sort()

print("\nЗадание 2 - Соритирвка")


def sort_to_max(origin_list):
    print("Исходный массив \n", origin_list)
    n = 1
    while n < len(origin_list):
        for i in range(len(origin_list) - n):
            if origin_list[i] > origin_list[i + 1]:
                origin_list[i], origin_list[i + 1] = origin_list[i + 1], origin_list[i]
        n += 1
    print("Отсортированный массив \n", origin_list)


sort_to_max([2, 10, -12, 2.5, 20, -11, 4, 4, 0])

# Задача-3:
# Напишите собственную реализацию стандартной функции filter.
# Разумеется, внутри нельзя использовать саму функцию filter.

print("\nЗадание 3 - Фильтер")

def labmda_fun(lamda, mass):
    print("Исходный массив \n", mass)
    origin_list = []
    for i in mass:
        if lamda(i) == True:
            origin_list.append(i)
    print("Отфильтрованный массив \n", origin_list)

mass = [2, 10, -12, 2.5, 20, -11, 4, 4, 0]
labmda_fun(lambda x: x <= 4, mass)

# Задача-4:
# Даны четыре точки А1(х1, у1), А2(x2 ,у2), А3(x3 , у3), А4(х4, у4).
# Определить, будут ли они вершинами параллелограмма.

print("\nЗадание 4 - Определение параллелограмма")
import math
dots = [[0, 2],
        [2, 4],
        [4, 2],
        [2, 0]]


def paral(dots):
    d1 = math.sqrt((dots[1][0] - dots[0][0])**2 + (dots[1][1] - dots[0][1])**2)
    d2 = math.sqrt((dots[2][0] - dots[1][0]) ** 2 + (dots[2][1] - dots[1][1]) ** 2)
    d3 = math.sqrt((dots[3][0] - dots[2][0]) ** 2 + (dots[3][1] - dots[2][1]) ** 2)
    d4 = math.sqrt((dots[0][0] - dots[3][0]) ** 2 + (dots[0][1] - dots[3][1]) ** 2)
    #print(d1, '', d2, '', d3, '', d4, '', )
    if (d1 == d3 and d2 == d4):
        return print("Данные точки состовляют параллелограмм")
    else:
        return print("Данные точки не состовляют параллелограмм")

paral(dots)
